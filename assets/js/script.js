'use strict';

const CURRENT   = 'current';
const CARROUSEL = document.getElementById('movie__pics');

var imagens     = CARROUSEL.querySelectorAll('.movie__pics__list .movie__pics__item');
var imagemAtiva = document.getElementById('movie__pic__current');
var coisa;

// transformando imagens de NodeList para array
imagens = Array.prototype.slice.call(imagens);

// funcao que muda a imagem no click do thumbnail
var mudaImagem = function(e) {
    e = e || window.event;
    var target = e.target || e.srcElement;
    var item = {};

    // selecinando o li da lista de thumbnails
    if( target.localName == "img" ) {
        item = target.parentElement;
    } else {
        item = target;
    }

    // adicionando a classe de ativo no item clicado e removendo da anterior
    if( !item.classList.contains(CURRENT) ) {
        for(var i = 0; i < imagens.length; i++) {
            if( imagens[i].classList.contains(CURRENT) ) {
                imagens[i].classList.remove(CURRENT);
            }
        }

        item.classList.toggle('current');
        imagemAtiva.src = item.firstElementChild.src;
    }

    //alert(imagemAtiva.src);
}

// adicionando evento no carrossel
CARROUSEL.addEventListener('click', mudaImagem(e), false);